document.addEventListener("mousemove", function(event) {
    let x = event.pageX;
    let y = event.pageY;
    let cursor = document.getElementById("cursor");
    cursor.style.top = y + "px";
    cursor.style.left = x + "px";
});


let buttons = document.querySelectorAll("button, a");
let cursor = document.getElementById("cursor");

Array.from(buttons).forEach(function(button) {
    button.addEventListener("mouseover", function(event) {
        cursor.classList.add("cursor-small");
    });
    button.addEventListener("mouseout", function(event) {
        cursor.classList.remove("cursor-small");
    });
});