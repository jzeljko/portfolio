const personForm = document.getElementById("person-form");
const personTable = document.getElementById("person-list");
const deleteLast = document.getElementById("deleteLast");
const deleteAll = document.getElementById("deleteAll");
const sortByName = document.getElementById("sortByLastName");
const sortByAge = document.getElementById("sortByAge");
let personArray = [];


personForm.addEventListener("submit", addPerson);


function addPerson(e) {
  e.preventDefault();
  let name = document.getElementById("name").value;
  let lastname = document.getElementById("lastname").value;
  let yearOfBirth = Number(document.getElementById("year-of-birth").value);
  let person = {
    name,
    lastname,
    yearOfBirth: new Date(yearOfBirth + '-01-01'),

  get age() {
    const difference = new Date (Date.now() - this.yearOfBirth);
    return Math.abs(difference.getUTCFullYear() -1970);
   },
   set age(age) {
    const yearOfBirth = new Date();
    yearOfBirth.setFullYear(yearOfBirth.getUTCFullYear()-age);
    this.yearOfBirth = yearOfBirth;
   }
  }

  personArray.push(person);
  document.getElementById("name").value = "";
  document.getElementById("lastname").value = "";
  document.getElementById("year-of-birth").value = "";
  render();
}




function render() {
  personTable.innerHTML = "";

  for (const person of personArray) {
    const row = document.createElement("tr");
    const nameCol = document.createElement("td");
    const lastNameCol = document.createElement("td");
    const ageCol = document.createElement("td");
    const changeAge = document.createElement("BUTTON");
    const deletePerson = document.createElement("BUTTON");

    nameCol.innerText = person.name;
    lastNameCol.innerText = person.lastname;
    ageCol.innerText = person.age +" (Geburtsjahr: "+person.yearOfBirth.getUTCFullYear()+")";
    changeAge.innerHTML= "Alter ändern";
    deletePerson.innerHTML= "Löschen";

    row.appendChild(nameCol);
    row.appendChild(lastNameCol);
    row.appendChild(ageCol);
    row.appendChild(changeAge);
    row.appendChild(deletePerson);

    personTable.appendChild(row);
  }
}

deleteLast.addEventListener("click", killChild)

function killChild() {
    personArray.pop()
    render();
}

deleteAll.addEventListener("click", killAllChildren)

function killAllChildren() {
  personArray = [];
  render();
}

sortByAge.addEventListener("click", sortAge)

function sortAge() {
  personArray.sort(function(a, b) {
    return parseFloat(a.yearOfBirth) - parseFloat(b.yearOfBirth);
});
render();
}

sortByName.addEventListener("click", sortname)

function sortname() {
  personArray.sort(function(a, b) {
    return a.lastname.localeCompare(b.lastname);
});
render();
}

