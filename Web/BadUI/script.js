document.querySelector(".buttonenter").addEventListener("click", function () {
  document.getElementById("content").style.display = "block";
  document.querySelector(".buttonenter").style.display = "none";
  document.body.style.background = "none";
  document.body.style.background =
    "repeating-linear-gradient(45deg, rgba(247, 110, 110, 0.2), rgba(255, 201, 201, 0.2) 10px, rgba(254, 254, 254, 0.3) 10px, rgba(255, 255, 255, 0.3) 20px)";
  setTimeout(function () {
    let result = confirm(
      "Do you have a desire to persist with the operation of annoyingSpinning.exe, despite the assumption that a OK response implies discontinuation?"
    );
    if (result) {
      document.querySelector(".buttonenter").style.animation =
        "spin 0.5s linear infinite";
      setTimeout(function () {
        let result2 = confirm(
          "Do you still have a desire to persist with the operation of annoyingSpinning.exe, despite the assumption that a OK response implies discontinuation?"
        );
        if (result2) {
          document.querySelector("body").style.animation =
            "spin 0.1s linear infinite";
        } else {
          document.querySelector("body").style.animation = "none";
        }
      }, 20000);
    } else {
      document.querySelector("body").style.animation = "none";
    }
    document.addEventListener("keydown", function (event) {
      if (event.key === "l") {
        document.querySelector("body").style.animation = "none";
      }
    });
  }, 10000);
});

let randomTime = Math.floor(Math.random() * (150000 - 100000 + 1)) + 10000;
let spinInterval = setInterval(function () {
  document.body.style.animation = "spin 2s linear infinite";
  let result = confirm(
    "Do you want to reactivate the disabled annoyingSpinning.exe? Or deactivate it again?"
  );
  if (result) {
    clearInterval(spinInterval);
    document.body.style.animation = "none";
  }
}, randomTime);

document.querySelector("#oneCookie").addEventListener("click", function () {
  let newElement = document.createElement("div");
  newElement.textContent = "MORE";
  newElement.classList.add("cookies");
  newElement.classList.add("moreCookies");
  document.querySelector(".cookies").appendChild(newElement);
});

document.querySelector(".giveAllCookies").addEventListener("click", function () {
  document.querySelectorAll(".cookies").forEach(function(el) {
    el.style.display = "none";
  });
});

const messages = [
  "Give us your Google Review!",
  "Like us on Facebook!",
  "Follow us on Twitter!",
  "Follow us on Instagram!",
  "Give us a Like on Facebook!",
  "Buy me a game on Steam!"
];

let messageIndex = 0;

setInterval(function () {
  window.alert(messages[messageIndex]);
  messageIndex = (messageIndex + 1) % messages.length;
  setTimeout(function () {
    let randomInterval = 10000 + 20000 * Math.random();
    setInterval(function () {
      window.alert(messages[messageIndex]);
      messageIndex = (messageIndex + 1) % messages.length;
    }, randomInterval);
  },)
}, 10000);

let audio = new Audio("alert.mp3");
let timer;
let timerRunning = true;
document.querySelector(".yugeButton").addEventListener("click", function () {
  document.querySelector("#regist").style.display = "block";
  document.querySelector(".yugeButton").style.display = "none";
  document.querySelector(".yugeButtonTwo").style.display = "block";
  document.querySelector("#login").style.display = "none";

  audio.play();

  let start = new Date();
  timer = setInterval(function () {
    let elapsedTime = (Date.now() - start) / 1000;
    console.log("Elapsed time: " + elapsedTime + " seconds");
  }, 1000);

  setTimeout(function () {
    if (timerRunning) {
      clearInterval(timer);
      document.body.innerHTML = "";
      console.log("Register failed!");
      let errorDiv = document.createElement("div");
      errorDiv.innerHTML = "Register failed! Try again!";
      errorDiv.style.position = "absolute";
      errorDiv.style.top = "100%";
      errorDiv.style.padding = "20px";
      errorDiv.style.color = "black";
      document.body.appendChild(errorDiv);
      let audio = new Audio("Gameover.mp3");
      audio.play();
      let rectangle = document.createElement("div");
      rectangle.style.width = "100px";
      rectangle.style.height = "50px";
      rectangle.style.backgroundColor = "red";
      rectangle.style.top = "50%";
      rectangle.style.left = "50%";
      rectangle.style.transform = "translate(-50%, -50%)";
      rectangle.style.position = "absolute";
      document.body.appendChild(rectangle);
  rectangle.addEventListener("click", function() {
    location.reload();
});
    }
  }, 30000);
});

const inputs = document.querySelectorAll("#regist input:not([type=submit])");

inputs.forEach((input) => {
  input.tabIndex = -1;
  let displayed = false;
  input.addEventListener("blur", (e) => {
    if (!displayed) {
      displayed = true;
      alert("Is this correct: " + input.value + " ? Click OK to continue");
    }
  });
});

function validateUsername() {
  let name = document.getElementById("name").value;
  let firstName = document.getElementById("firstname").value;
  let phoneNumber = document.getElementById("number").value;
  let username = document.getElementById("username").value;
  let password = document.getElementById("password").value;

  let hasNumber = /\d/;
  let hasSpecial = /[!@#$%^&*]/;

  if (name.length < 3) {
    alert("Name must contain at least 3 characters!");
    return false;
  }
  if (firstName.length < 3) {
    alert("First name must contain at least 3 characters!");
    return false;
  }
  if (isNaN(phoneNumber) || phoneNumber.length < 10) {
    alert("Phone number must contain only numbers and at least 10 characters!");
    return false;
  }
  if (!username.match(hasNumber) && !username.match(hasSpecial)) {
    alert("Username has to contain at least one number or special character!");
    return false;
  }
  if (password.length < 8) {
    alert("Password must contain at least 8 characters!");
    return false;
  }
  document.querySelector("#regist").style.display = "none";
  return true;
}

let UserArray = [];
let User = {};

document.getElementById("regist").addEventListener("submit", function (event) {
  event.preventDefault();

  User.name = document.getElementById("name").value;
  User.firstname = document.getElementById("firstname").value;
  User.number = document.getElementById("number").value;
  User.username = document.getElementById("username").value;
  User.password = document.getElementById("password").value;

  if (validateUsername()) {
    UserArray.push(User);
    timerRunning = false;
    clearInterval(timer);
    audio.pause();
  }

  console.log(UserArray);
});

document.querySelector(".yugeButtonTwo").addEventListener("click", function () {
  document.querySelector("#login").style.display = "block";
  document.querySelector(".yugeButtonTwo").style.display = "none";
  document.querySelector("#regist").style.display = "none";
  document.querySelector(".yugeButton").style.display = "block";
});

document.getElementById("login").addEventListener("submit", function (event) {
  event.preventDefault();
  let enteredUsername = document.getElementById("username").value;
  let enteredPassword = document.getElementById("password").value;

  let user = UserArray.find(
    (user) => user.username === enteredUsername && user.password === enteredPassword
  );

  if (
    user && enteredUsername === user.username && enteredPassword === user.password
  ) {
    console.log("Login sucessfull!");
    document.body.innerHTML = "";
    let button = document.createElement("button");
    button.innerHTML = "Enter";
    button.classList.add("enter-button");
    document.body.appendChild(button);
    document.querySelector(".enter-button").addEventListener("click", function () {
        let video = document.createElement("video");
        video.src = "Rickroll.mp4";
        video.autoplay = true;
        video.style.width = "100%";
        video.style.height = "100%";
        video.style.objectFit = "fill";
        video.style.position = "fixed";
        video.style.top = 0;
        video.style.left = 0;
        document.body.appendChild(video);
      });
  } else {
    document.body.innerHTML = "";
    console.log("Login failed!");
    let errorDiv = document.createElement("div");
    errorDiv.innerHTML = "Login failed! Try again!";
    errorDiv.style.position = "absolute";
    errorDiv.style.top = "100%";
    errorDiv.style.padding = "20px";
    errorDiv.style.color = "black";
    document.body.appendChild(errorDiv);
    let audio = new Audio("Gameover.mp3");
    audio.play();
    let rectangle = document.createElement("div");
    rectangle.style.width = "100px";
    rectangle.style.height = "50px";
    rectangle.style.backgroundColor = "red";
    rectangle.style.top = "50%";
    rectangle.style.left = "50%";
    rectangle.style.transform = "translate(-50%, -50%)";
    rectangle.style.position = "absolute";
    document.body.appendChild(rectangle);
    rectangle.addEventListener("click", function() {
      location.reload();
    });
  }
});

console.log(UserArray);
