import java.util.Scanner;

public class Main {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        System.out.print("Choose your Number of Rows = ");
        int rows = sc.nextInt();

        for (int i = 1 ; i <= rows; i++ )
        {
            for (int j = 0 ; j < rows - i; j++ )
            {
                System.out.print("  ");
            }
            for (int k = 0 ; k < (i * 2) - 1; k++ )
            {
                System.out.print(i +" ");
            }
            System.out.println();
        }
    }
}