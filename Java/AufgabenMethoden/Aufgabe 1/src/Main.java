public class Main {

    static int a = 10;
    static int b = 5;

    public static void main(String[] args) {

        Results (summe(), "Summe");
        Results (division(), "Division");
        Results (quot(), "Quotient");
        Results (mini(), "Minimum");
        Results (maxi(), "Minimum");
    }

    public static int summe () {
        int result = a +b;

        return result;
    }

    public static int division () {
        int result = a/b;

        return result;
    }

    public static int quot () {
        int result = b/a;

        return result;
    }

    public static int mini () {
        int result = Math.min(a,b);

        return result;
    }

    public static int maxi () {
        int result = Math.max(a,b);

        return result;
    }
    public static void Results (int result, String math) {
        System.out.println("Das Ergebnis " + math +" lautet: " + result);
    }
}