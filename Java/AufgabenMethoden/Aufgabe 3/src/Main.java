public class Main {
    static int sidea = 5;
    static int sideb = 4;
    static int sidec = 3;

    public static void main(String[] args) {

    ausgabe (volume(), "Volumen");
    ausgabe (surface(), "Oberfläche");
    ausgabe (length(), "Länge");
    }

    public static int volume () {
        int result = sidea*sideb*sidec;

        return result;
    }

    public static int surface () {
        int result = (sidea * sidec + sidea*sideb + sideb*sidec);

        return result;
    }
    public static int length () {
        int result = 3 * (sidea*sidea);

        return result;
    }
    public static void ausgabe (int result, String Rechnung) {
        System.out.println("Das Ergebnis von " + Rechnung +" lautet: " + result );
    }
}