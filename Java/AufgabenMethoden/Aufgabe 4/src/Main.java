public class Main {
    public static boolean prim(int n) {
        boolean primzahl = true;
        if ((n % 2) == 0 || (n % 3) == 0) {
            primzahl = false;
        }
        return primzahl;
    }

    public static void main(String[] args) {
        for (int nummer = 2; nummer < 50; nummer++) {
            System.out.println("Die Zahl " + nummer + " ist eine Primzahl? " + prim(nummer));
        }
    }
}

